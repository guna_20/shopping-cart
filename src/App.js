import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { CartProvider } from 'react-use-cart';
import Layout from './components/Layout';
import Login from './components/Auth/Login';
import Register from './components/Auth/Register';
import Home from './components/Home/Home';
import Product from './components/Product/Product';

function App() {
  return (
    <div>
      {/* <CartProvider>
        <Routes>
          {!authenticated && <Route path="/login" element={<Login />} />}
          <Route
            path="/"
            element={
              <RequireAuth>
                <Layout />
              </RequireAuth>
            }
          >
            <Route path="/login" element={<Login />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route path="/product" element={<Product />}></Route>
            <Route path="/cart" element={<Cart />}></Route>
          </Route>

          <Route
            path="*"
            element={<Navigate to={authenticated ? '/' : '/login'} />}
          />
        </Routes>
      </CartProvider> */}
      <CartProvider>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route path="/" index element={<Login />} />
            <Route path="/register" index element={<Register />} />
            <Route path="/home" element={<Home />} />
            <Route path="/product" element={<Product />} />
          </Route>
        </Routes>
      </CartProvider>
    </div>
  );
}

export default App;
