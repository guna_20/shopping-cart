import React, { useState } from "react";
import { Modal, Row, Col, Container, Button } from "react-bootstrap";
import { useCart } from "react-use-cart";
import lowest_price from "../../static/images/lowest_price.png";

export default function AuthModal(props) {
  const [quantity, setQuantity] = useState(0);

  const increment = () => {
    setQuantity((prevCount) => prevCount + 1);
  };
  const decrement = () => {
    setQuantity((prevCount) => prevCount - 1);
  };

  const {
    isEmpty,
    items,
    totalItems,
    totalUniqueItems,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  // if(isEmpty) return <h1 className='text-center'>No items in your cart</h1>

  return (
    <div className="out-container">
      <Modal {...props} aria-labelledby="contained-modal-title-vcenter">
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            My Cart ({totalUniqueItems} items)
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="show-grid">
          <Container>
            <Row>
              <Col xs={12}>{isEmpty}empty cart</Col>
            </Row>

            {/* <div className="col-auto">
              <button className="btn btn-pink m-2" onClick={() => emptyCart()}>
                Clear Cart
              </button>
            </div> */}
            <Row style={{ background: "#FAF9F6" }}>
              <Col md={5}>
                <img src={lowest_price} alt="lowest" />
              </Col>
              You won't find it cheaper anywhere
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <h5>Promo Code can be applied on payment page</h5>
          <br />
          <br />
          <butoon class="btn btn-login">Proceed To payment</butoon>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
