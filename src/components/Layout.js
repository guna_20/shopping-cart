import React, { useState, useEffect } from "react";
import logo_sabka from "../assets/img/logo_sabka.png";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { useCart } from "react-use-cart";
import Modal from "../components/Cart/Modal";
import EmptyModal from "../components/Cart/EmptyModal";

export default function Layout() {
  const [modalShow, setModalShow] = React.useState(false);
  const [toggleSidebar, setToggleSideBar] = useState(false);

  const select = (el, all = false) => {
    el = el.trim();
    if (all) {
      return [document.querySelectorAll(el)];
    } else {
      return document.querySelector(el);
    }
  };

  const on = (type, el, listener, all = false) => {
    if (all) {
      select(el, all).forEach((e) => e.addEventListener(type, listener));
    } else {
      select(el, all).addEventListener(type, listener);
    }
  };
  const onscroll = (el, listener) => {
    el.addEventListener("scroll", listener);
  };
  const {
    isEmpty,
    totalItems,
    totalUniqueItems,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  useEffect(() => {
    if (select(".toggle-sidebar-btn")) {
      on("click", ".toggle-sidebar-btn", function (e) {
        select("body").classList.toggle("toggle-sidebar");
      });
    }
    let navbarlinks = select("#navbar .scrollto", true);
    const navbarlinksActive = () => {
      let position = window.scrollY + 200;
      navbarlinks.forEach((navbarlink) => {
        if (!navbarlink.hash) return;
        let section = select(navbarlink.hash);
        if (!section) return;
        if (
          position >= section.offsetTop &&
          position <= section.offsetTop + section.offsetHeight
        ) {
          navbarlink.classList.add("active");
        } else {
          navbarlink.classList.remove("active");
        }
      });
    };
    window.addEventListener("load", navbarlinksActive);
    onscroll(document, navbarlinksActive);
  }, []);

  return (
    <>
      <div>
        <header
          id="header"
          className="header fixed-top d-flex align-items-center"
        >
          <div className="d-flex align-items-center justify-content-between">
            <a href="" className="d-flex align-items-center">
              <img src={logo_sabka} alt="" />
            </a>
            <i className="bi bi-list toggle-sidebar-btn"></i>
          </div>

          <nav class="header__menu">
            <ul>
              <li class="active">
                <a href="/home">Home</a>
              </li>
              <li>
                <a href="/product">Products</a>
              </li>
            </ul>
          </nav>
          <nav className="header-nav ms-auto">
            <a href="/" class="header-end">
              Login
            </a>
            <a href="/register" class="header-end" style={{ marginLeft: 10 }}>
              Register
            </a>
            <ul className="d-flex align-items-center">
              <li className="nav-item dropdown pe-3 m-3">
                <div class="cart" style={{ backgroundColor: "lightgray" }}>
                  <button
                    type="button"
                    id="cart-btn"
                    onClick={() => setModalShow(true)}
                  >
                    <i
                      class="fas fa-shopping-cart "
                      style={{ color: "deeppink" }}
                    ></i>
                  </button>
                  {totalUniqueItems} items
                  {/* <EmptyModal show={modalShow} onHide={(e)=>setModalShow(false)}/> */}
                  <Modal show={modalShow} onHide={(e) => setModalShow(false)} />
                </div>
              </li>
            </ul>
          </nav>
        </header>
      </div>
      {/* <div>
        <aside id="sidebar" className="sidebar">
          <nav>
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <Link className="nav-link " to="/">
                  <i class="bi bi-bookmark"></i>
                  Introduction
                </Link>
              </li>
            </ul>
          </nav>
        </aside> */}
      <Outlet />
      {/* </div> */}
      <footer id="footer" class="footer">
        <div class="copyright text-center">
          Copyright &copy; 2011-2018{" "}
          <span>Sabka Bazaar Grocery Supplies Pvt. Ltd</span>
        </div>
      </footer>
    </>
  );
}
