import React, { useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';

export default function Register(props) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmpassword, setConfirmPassword] = useState('');

  const HandleSignUp = () => {
    let item = { firstName, lastName, password, confirmpassword };
    console.log(item);
  };
  return (
    <div>
      <div>
        <section id="hero" class="hero d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h4
                  data-aos="fade-up"
                  style={{ fontSize: '50', color: 'deeppink' }}
                >
                  Sign Up{' '}
                </h4>
                <h5 data-aos="fade-up" data-aos-delay="400">
                  We do not share your personal details with anyone.
                </h5>
              </div>
              <div class="col-lg-6 hero-img">
                <div class=" mb-3" style={{ width: 444 }}>
                  <div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 350 }}
                        type="text"
                        value={firstName}
                        onChange={(e) => setFirstName(e.target.value)}
                      >
                        First Name
                      </label>
                      <input type="text" className="form-control" />
                    </div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 350 }}
                        type="text"
                        value={lastName}
                        onChange={(e) => setLastName(e.target.value)}
                      >
                        Last Name
                      </label>
                      <input type="text" className="form-control" />
                    </div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 360 }}
                        type="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                      >
                        Password
                      </label>
                      <input type="password" className="form-control" />
                    </div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 300 }}
                        type="password"
                        value={confirmpassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                      >
                        Confirm Password
                      </label>
                      <input type="password" className="form-control" />
                    </div>

                    <div className="text-center">
                      <Link
                        to="/"
                        style={{ marginTop: 20 }}
                        className="btn btn-login"
                        onClick={HandleSignUp}
                      >
                        Sign Up
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
