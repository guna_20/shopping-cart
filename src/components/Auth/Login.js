import React, { useState } from 'react';
import { Link, useNavigate, useLocation } from 'react-router-dom';

export default function Login(props) {
  const [userName, setUserName] = useState('admin1');
  const [password, setPassword] = useState('user123');

  const navigate = useNavigate();

  function authUser() {
    const user = JSON.parse(sessionStorage.getItem('user'));
    navigate('/home');
    return { Authorization: 'Bearer ' + user.accessToken };
  }

  return (
    <div>
      <div>
        <section id="hero" class="hero d-flex align-items-center">
          <div class="container">
            <div class="row">
              <div class="col-lg-6 d-flex flex-column justify-content-center">
                <h4
                  data-aos="fade-up"
                  style={{ fontSize: '50', color: 'deeppink' }}
                >
                  Login{' '}
                </h4>
                <h5 data-aos="fade-up" data-aos-delay="400">
                  Get access to your Orders, Wishlist and Recommendations
                </h5>
              </div>
              <div class="col-lg-6 hero-img">
                <div class=" mb-3" style={{ width: 444 }}>
                  <div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 400 }}
                      >
                        Email
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        
                        onChange={(e) => setUserName(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <label
                        className="form-label"
                        style={{ marginRight: 400 }}
                      >
                        Password
                      </label>
                      <input
                        type="password"
                        className="form-control"
                       
                        onChange={(e) => setPassword(e.target.value)}
                      />
                    </div>

                    <div className="text-center">
                      <Link
                        to="/"
                        style={{ marginTop: 20 }}
                        className="btn btn-login"
                        onClick={(e) => authUser(e)}
                      >
                        Login
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
