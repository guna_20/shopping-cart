import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import ProductDetails from '../../server/products/index.get';
import { useCart } from 'react-use-cart';

export default function Product() {
  const [items, setItems] = useState(ProductDetails);
  const { addItem } = useCart();

  const filterItem = (categoryitem) => {
    const updateItem = ProductDetails.filter((currentItem) => {
      return currentItem.category === categoryitem;
    });
    setItems(updateItem);
  };

  return (
    <>
      <div>
        <aside id="sidebar" className="sidebar">
          <nav>
            <ul className="sidebar-nav" id="sidebar-nav">
              <li className="nav-item">
                <Link
                  className="nav-link collapsed "
                  to=""
                  onClick={() => {
                    filterItem('5b6899953d1a866534f516e2');
                  }}
                >
                  Fruits and Vegetables
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link collapsed"
                  to=""
                  onClick={() => {
                    filterItem('5b6899123d1a866534f516de');
                  }}
                >
                  Bakery Cakes and Dairy
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link collapsed"
                  to=""
                  onClick={() => {
                    filterItem('5b675e5e5936635728f9fc30');
                  }}
                >
                  Beverages
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link collapsed"
                  to=""
                  onClick={() => {
                    filterItem('5b68994e3d1a866534f516df');
                  }}
                >
                  Beauty and Hygiene
                </Link>
              </li>
              <li className="nav-item">
                <Link
                  className="nav-link collapsed"
                  to=""
                  onClick={() => {
                    filterItem('5b6899683d1a866534f516e0');
                  }}
                >
                  Baby Care
                </Link>
              </li>
            </ul>
          </nav>
        </aside>

        <div className="wrapper">
          {items.map((data, index) => {
            return (
              <div class="wrapper">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">{data.name}</h5>
                    <img src={data.imageURL} alt="..." />
                    <br />
                    <br />
                    <p class="card-text">{data.description}</p>
                    <div class="row card-text home-buy">
                      <div class="col ">
                        <p>MRP Rs. {data.price}</p>
                      </div>
                      <div class="col ">
                        <button
                          className="btn-pink"
                          onClick={() => addItem(data)}
                        >
                          Buy Now
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
}
